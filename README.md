# Akka http based file storage server #


### What is this repository for? ###

Akka http based file storage. Provides binary api for saving and loading files, folders and keeping separate user profiles 

### How do I get set up? ###

figure it out.

* Configuration:
resources folder has all the files required.

* Dependencies:
see build.sbt

* Database configuration:
Right now it uses mapdb, which is integrated database, no setup required, db will be created automatically.

* How to run tests:
tests are coming. They are not coming in your lifetime, but they are coming.

* Deployment instructions:
build jar and run it, no special deployment required

### Contribution guidelines ###

fork it. Or be nice and create pull request with changes and tests for them.

### Who do I talk to? ###

Anybody you can find.