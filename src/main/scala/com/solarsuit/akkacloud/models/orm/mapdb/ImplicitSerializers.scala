package com.solarsuit.akkacloud.models.orm.mapdb

import org.mapdb.serializer._
import org.mapdb.{DataInput2, DataOutput2, Serializer}

/**
  * Created by arsenii on 03.04.17.
  */
case object ImplicitSerializers {

  lazy implicit val CHAR = new SerializerChar

  lazy implicit val STRING = new SerializerString

  lazy implicit val JBOOLEAN = new SerializerBoolean

  implicit object BOOLEAN extends Serializer[Boolean] {
    override def serialize(out: DataOutput2, value: Boolean): Unit = out.writeBoolean(value)

    override def deserialize(input: DataInput2, available: Int): Boolean = input.readBoolean()
  }

  /*  lazy implicit val STRING_ORIGHASH = new SerializerStringOrigHash

    lazy implicit val STRING_DELTA = new SerializerStringDelta

    lazy implicit val STRING_DELTA2 = new SerializerStringDelta2

    lazy implicit val STRING_INTERN = new SerializerStringIntern

    lazy implicit val STRING_ASCII = new SerializerStringAscii

    lazy implicit val STRING_NOSIZE = new SerializerStringNoSize*/

  lazy implicit val LONG = new SerializerLong

  lazy implicit val LONG_PACKED = new SerializerLongPacked

  lazy implicit val LONG_DELTA = new SerializerLongDelta

  lazy implicit val INTEGER = new SerializerInteger

  lazy implicit val INTEGER_PACKED = new SerializerIntegerPacked

  lazy implicit val INTEGER_DELTA = new SerializerIntegerDelta

  lazy implicit val RECID = new SerializerRecid

  lazy implicit val RECID_ARRAY = new SerializerRecidArray

  lazy implicit val ILLEGAL_ACCESS = new SerializerIllegalAccess

  lazy implicit val BYTE_ARRAY = new SerializerByteArray

  lazy implicit val BYTE_ARRAY_DELTA = new SerializerByteArrayDelta
  lazy implicit val BYTE_ARRAY_DELTA2 = new SerializerByteArrayDelta2

  lazy implicit val BYTE_ARRAY_NOSIZE = new SerializerByteArrayNoSize

  lazy implicit val CHAR_ARRAY = new SerializerCharArray

  lazy implicit val INT_ARRAY = new SerializerIntArray

  lazy implicit val LONG_ARRAY = new SerializerLongArray

  lazy implicit val DOUBLE_ARRAY = new SerializerDoubleArray

  lazy implicit val JAVA = new SerializerJava

  lazy implicit val ELSA = new SerializerElsa

  lazy implicit val UUID = new SerializerUUID

  lazy implicit val BYTE = new SerializerByte

  lazy implicit val FLOAT = new SerializerFloat

  lazy implicit val DOUBLE = new SerializerDouble

  lazy implicit val SHORT = new SerializerShort

  lazy implicit val SHORT_ARRAY = new SerializerShortArray

  lazy implicit val FLOAT_ARRAY = new SerializerFloatArray

  lazy implicit val BIG_INTEGER = new SerializerBigInteger

  lazy implicit val BIG_DECIMAL = new SerializerBigDecimal

  lazy implicit val CLASS = new SerializerClass

  lazy implicit val DATE = new SerializerDate


  def apply[A: Serializer]: Serializer[A] = implicitly[Serializer[A]]

  def apply[A: Serializer, B: Serializer]: Tuple2Serializer[A, B] = new Tuple2Serializer[A, B]

  def apply[A: Serializer, B: Serializer, C: Serializer]: Tuple3Serializer[A, B, C] = new Tuple3Serializer[A, B, C]

}


class Tuple2Serializer[A, B](implicit s1: Serializer[A], s2: Serializer[B]) extends Serializer[(A, B)] {

  override def serialize(out: DataOutput2, value: (A, B)): Unit = {
    s1.serialize(out, value._1)
    s2.serialize(out, value._2)
  }

  override def deserialize(input: DataInput2, available: Int): (A, B) = {
    (s1.deserialize(input, available), s2.deserialize(input, available))
  }
}

class Tuple3Serializer[A, B, C](implicit s1: Serializer[A], s2: Serializer[B], s3: Serializer[C]) extends Serializer[(A, B, C)] {

  override def serialize(out: DataOutput2, value: (A, B, C)): Unit = {
    s1.serialize(out, value._1)
    s2.serialize(out, value._2)
    s3.serialize(out, value._3)
  }

  override def deserialize(input: DataInput2, available: Int): (A, B, C) = {
    (
      s1.deserialize(input, available),
      s2.deserialize(input, available),
      s3.deserialize(input, available)
    )
  }
}
