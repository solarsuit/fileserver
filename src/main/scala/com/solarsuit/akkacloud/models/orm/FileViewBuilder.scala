package com.solarsuit.akkacloud.models.orm

import java.nio.file.Path

import better.files._

object FileViewBuilder {
  def apply(file: File, rootPath: Path): FileView = FileViewBuilder(file, rootPath, FileMapping.provider.seekByPath(file.toString))

  def apply(file: File, rootPath: Path, mapping: Option[FileMapping]): FileView =
    new FileView(
      rootPath.relativize(file.path).toString,
      file.isDirectory,
      file.attributes.lastModifiedTime().toMillis,
      mapping.map("/download/" + _.id),
      mapping.map(_.contentType).getOrElse(FileMapping.provider.getMimeType(file)).toString()
    )
}

