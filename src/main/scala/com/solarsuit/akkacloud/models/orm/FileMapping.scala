package com.solarsuit.akkacloud.models.orm

import akka.http.scaladsl.model.ContentType
import better.files._
import com.solarsuit.akkacloud.WebServer
import com.solarsuit.akkacloud.models.orm.common.FileMappingProvider
import com.solarsuit.akkacloud.models.orm.mapdb.MapDbFile

import scala.util.Try

/**
  * Created by arsenii on 08.03.17.
  */
case class FileMapping(id: String, path: String, contentType: ContentType) {

  lazy val getFile: File = WebServer.storagePath / path

  def delete(): Try[Unit] = {
    FileMapping.provider.delete(this)
  }

  def save(): Try[Unit] = {
    FileMapping.provider.save(this)
  }

}


object FileMapping {

  var provider: FileMappingProvider = MapDbFile

}






