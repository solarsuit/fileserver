package com.solarsuit.akkacloud.models.orm.common

import scala.util.Try

/**
  * Created by arsenii on 03.04.17.
  */
trait EntityProvider[Entity, Key, CreationData] {

  def delete(f: Entity): Try[Unit]

  def save(e: Entity): Try[Unit]

  def apply(id: Key): Option[Entity]

  def create(data: CreationData): Option[Entity] = {
    val newVal = _create(data)
    val saveResult = save(newVal)
    saveResult.fold(_ => None, _ => Some(newVal))
  }

  protected def _create(data: CreationData): Entity

}
