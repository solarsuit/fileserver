package com.solarsuit.akkacloud.models.orm.mapdb

import java.security.SecureRandom
import java.util.Base64

import com.solarsuit.akkacloud.models.orm.FileMapping
import com.solarsuit.akkacloud.models.orm.common.FileMappingProvider
import com.solarsuit.akkacloud.models.orm.mapdb.MapDbSchema._

import scala.util.Try

/**
  * Created by arsenii on 08.03.17.
  */
object MapDbFile extends FileMappingProvider {

  private lazy val random = new SecureRandom

  override def apply(id: String): Option[FileMapping] =
    Option(idToPath.get(id)).map[FileMapping](path => new FileMapping(id, path._1, parseMimeType(path._2)))

  override def delete(f: FileMapping): Try[Unit] = Try {
    pathToId.remove(f.path)
    idToPath.remove(f.id)
  }

  override def save(f: FileMapping): Try[Unit] = Try {
    pathToId.put(f.path, (f.id, f.contentType.toString))
    idToPath.put(f.id, (f.path, f.contentType.toString))
  }

  protected override def _create(path: String): FileMapping =
    new FileMapping(genId, removePrefix(path), getMimeType(path))

  override protected def _seekByPath(path: String): Option[FileMapping] =
    Option(pathToId.get(path)).map[FileMapping](id => new FileMapping(id._1, path, parseMimeType(id._2)))

  private def genId: String = {
    val bytes = new Array[Byte](15)
    random.nextBytes(bytes)
    val id = Base64.getUrlEncoder.encodeToString(bytes)
    this (id) match {
      case Some(_) => genId
      case None => id
    }
  }
}

