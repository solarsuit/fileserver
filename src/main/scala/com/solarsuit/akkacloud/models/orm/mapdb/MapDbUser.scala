package com.solarsuit.akkacloud.models.orm.mapdb

import java.security.SecureRandom
import java.util.Base64
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

import com.solarsuit.akkacloud.models.orm.User
import com.solarsuit.akkacloud.models.orm.User.{HASH_BYTE_SIZE, PBKDF2_ITERATIONS, SECURE_KEY_PROVIDER}
import com.solarsuit.akkacloud.models.orm.common.EntityProvider

import scala.util.Try

/**
  * Created by arsenii on 05.04.17.
  */
object MapDbUser extends EntityProvider[User, String, (String, String)] {

  override def delete(f: User): Try[Unit] = Try(MapDbSchema.users.remove(f.id))

  override def save(e: User): Try[Unit] = Try(MapDbSchema.users.put(e.id, (e.password, e.salt, e.enabled)))

  override def apply(id: String): Option[User] = Option(MapDbSchema.users.get(id)).map(e => new User(id, e._1, e._2, e._3))

  protected def _create(userData: (String, String)): User = {
    val random = SecureRandom.getInstanceStrong
    val salt = new Array[Byte](16)
    random.nextBytes(salt)
    val spec = new PBEKeySpec(userData._2.toCharArray, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE)
    val f = SecretKeyFactory.getInstance(SECURE_KEY_PROVIDER)
    val hash = f.generateSecret(spec).getEncoded
    val enc = Base64.getEncoder
    new User(userData._1, enc.encodeToString(hash), enc.encodeToString(salt), true)
    //TODO:add mail activation
  }
}
