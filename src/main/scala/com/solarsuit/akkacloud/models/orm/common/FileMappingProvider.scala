package com.solarsuit.akkacloud.models.orm.common

import akka.http.scaladsl.model.{ContentType, ContentTypes}
import better.files._
import com.solarsuit.akkacloud.WebServer
import com.solarsuit.akkacloud.models.orm.FileMapping

/**
  * Created by arsenii on 08.03.17.
  */

trait FileMappingProvider extends EntityProvider[FileMapping, String, String] {

  final def seekByPath(path: String): Option[FileMapping] = _seekByPath(removePrefix(path))

  protected final def removePrefix(path: String): String = path.replaceFirst(WebServer.storagePath, "")

  protected final def parseMimeType(value: String): ContentType =
    ContentType.parse(value).right.toOption.getOrElse(ContentTypes.`application/octet-stream`)

  protected def _seekByPath(path: String): Option[FileMapping]

  protected def getMimeType(path: String): ContentType = getMimeType(path.toFile)

  final def getMimeType(file: File): ContentType =
    file.contentType.flatMap(s => ContentType.parse(s).right.toOption).getOrElse(ContentTypes.`application/octet-stream`)

}
