package com.solarsuit.akkacloud.models.orm.mapdb

import java.io.File

import com.solarsuit.akkacloud.WebServer
import com.solarsuit.akkacloud.models.orm.User
import com.solarsuit.akkacloud.models.orm.mapdb.ImplicitSerializers._
import org.mapdb._

/**
  * Created by arsenii on 03.04.17.
  */
package object MapDbSchema {

  lazy val database: DB =
    DBMaker
      .fileDB(new File(WebServer.dbPath))
      .transactionEnable
      .make()

  lazy val idToPath: HTreeMap[String, (String, String)] = database
    .hashMap("IdToPath")
    .keySerializer(Serializer.STRING_ASCII)
    .valueSerializer(ImplicitSerializers[String, String])
    .createOrOpen()


  lazy val pathToId: HTreeMap[String, (String, String)] = database
    .hashMap("PathToId")
    .keySerializer(Serializer.STRING)
    .valueSerializer(ImplicitSerializers[String, String])
    .createOrOpen()

  lazy val users: HTreeMap[String, (String, String, Boolean)] = database
    .hashMap("Users")
    .keySerializer(Serializer.STRING)
    .valueSerializer(ImplicitSerializers[String, String, Boolean])
    .createOrOpen()

  def close() = {
    database.commit()
    database.close()
  }
}

object Utils {
  def main(args: Array[String]): Unit = {
    println("Hi")
    User.provider.create(("bell", "SomePasswordForTheTest"))
    User.provider.create("arsenii", "FixMeADrink")
    MapDbSchema.close()
    println("Bye")
  }
}
