package com.solarsuit.akkacloud.models.orm

import java.util.Base64
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

import akka.http.scaladsl.server.directives.Credentials
import com.solarsuit.akkacloud.models.orm.common.EntityProvider
import com.solarsuit.akkacloud.models.orm.mapdb.MapDbUser

/**
  * Created by arsenii on 05.04.17.
  */
case class User(id: String, password: String, salt: String, enabled: Boolean) {

  def delete(): Unit = User.provider.delete(this)
  def save(): Unit = User.provider.save(this)
}

object User {
  val SECURE_KEY_PROVIDER = "PBKDF2WithHmacSHA512" //TODO:move to config file
  val PBKDF2_ITERATIONS = 65536
  val HASH_BYTE_SIZE = 128

  var provider: EntityProvider[User, String, (String, String)] = MapDbUser

  def auth(cred: Credentials): Option[User] = {
    cred match {
      case Credentials.Missing => None
      case Credentials.Provided(id) =>
        provider(id).flatMap(user =>
          if (cred.asInstanceOf[Credentials.Provided].verify(user.password, hasher(user.salt)))
            Some(user)
          else
            None
        )
    }
  }

  def hasher(salt: String)(password: String): String = {
    val dec = Base64.getDecoder
    val spec = new PBEKeySpec(password.toCharArray, dec.decode(salt), User.PBKDF2_ITERATIONS, HASH_BYTE_SIZE)
    val f = SecretKeyFactory.getInstance(SECURE_KEY_PROVIDER)
    val hash = f.generateSecret(spec).getEncoded
    val enc = Base64.getEncoder
    enc.encodeToString(hash)
  }
}