package com.solarsuit.akkacloud

import scala.util.control.NonFatal

/**
  * Created by arsenii on 13.03.17.
  */
object NotTrowing {

  import java.util.logging.{LogManager, Logger}

  lazy val logger: Logger = LogManager.getLogManager.getLogger("NonThrowing")

  implicit def doNonThrowing[B](f: () => B): Option[B] = {
    try {
      Some(f())
    } catch {
      case NonFatal(e) =>
        logger.warning(e.toString)
        None
    }
  }

}
