package com.solarsuit.akkacloud.controllers.helpers

import java.net.URLEncoder

import akka.http.javadsl.model.headers.UserAgent
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.ContentDispositionTypes
import better.files.File
import com.solarsuit.akkacloud.models.orm.FileMapping

/**
  * Created by arsenii on 25.03.17.
  */

trait FileResponce {

  implicit def fileToTuple(f: File): (File, ContentType) = (f, FileMapping.provider.getMimeType(f))

  def fileToStreamingResponce(f: (File, ContentType), userAgent: Option[UserAgent] = None): HttpResponse = {
    if (f._1.isDirectory) {
      val fz = f._1.zip()
      HttpResponse(
        StatusCodes.OK,
        Vector(attachment(f._1.name + ".zip", userAgent)),
        HttpEntity.fromPath(MediaTypes.`application/zip`, fz.path)
      )
    } else {
      HttpResponse(
        StatusCodes.OK,
        Vector(attachment(f._1.name, userAgent)),
        HttpEntity.fromPath(f._2, f._1.path)
      )

    }
  }

  def attachment(filename: String, userAgent: Option[UserAgent]): HttpHeader =
    userAgent match {
      case Some(n) if n.value().toLowerCase.contains("mac") => new headers.`Content-Disposition`(ContentDispositionTypes.attachment,
        Map("filename" -> filename))
      case _ => new headers.`Content-Disposition`(ContentDispositionTypes.attachment,
        Map("filename*" -> encodeNonASCIIFilename(filename)))
    }


  private def encodeNonASCIIFilename(filename: String) = {
    "utf-8''" + URLEncoder.encode(filename, "utf-8").replace("+", "%20")
  }
}
