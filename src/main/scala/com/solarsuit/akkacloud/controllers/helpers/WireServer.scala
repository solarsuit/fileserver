package com.solarsuit.akkacloud.controllers.helpers

import java.nio.ByteBuffer

import autowire._
import boopickle.Default._
import com.solarsuit.akkacloud.controllers.ManageFiles
import com.solarsuit.akkacloud.models.orm.User

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class WireServer(user: User) extends Server[ByteBuffer, Pickler, Pickler] {

  implicit def toByteBuffer(array: Array[Byte]): ByteBuffer = ByteBuffer.wrap(array)

  private val routes: this.Router = this.route[Manage](new ManageFiles(user.id))

  override def read[Result](p: ByteBuffer)(implicit evidence$1: Pickler[Result]): Result = Unpickle[Result].fromBytes(p)

  override def write[Result](r: Result)(implicit evidence$2: Pickler[Result]): ByteBuffer = Pickle.intoBytes(r)

  def process(path: Seq[String], request: Array[Byte]): Future[Array[Byte]] = {
    val parsedRequest = Unpickle[Map[String, ByteBuffer]].fromBytes(request)
    routes(
      Core.Request(
        path,
        parsedRequest
      )
    ).map(buffer => {
      val data = Array.ofDim[Byte](buffer.remaining())
      buffer.get(data)
      data
    })
  }
}