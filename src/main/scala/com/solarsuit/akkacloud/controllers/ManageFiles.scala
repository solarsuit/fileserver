package com.solarsuit.akkacloud.controllers

import java.nio.file.{Path, Paths}

import akka.http.scaladsl.model.{HttpResponse, Multipart}
import akka.stream.Materializer
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink}
import better.files._
import com.solarsuit.akkacloud.WebServer
import com.solarsuit.akkacloud.controllers.helpers.FileResponce

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Created by arsenii on 15.04.17.
  */
class ManageFiles(username: String) extends ManageImpl with FileResponce {

  override protected lazy val curUserRootPath: Path = Paths.get(WebServer.storagePath, username)

  def download(path: String): Option[HttpResponse] = doIoWithFile(path)(file => fileToStreamingResponce(file))

  def getFile(path: String): Option[File] = doIoWithFile(path)(a => a)

  def partSink(path: List[String])(implicit materializer: Materializer): Sink[Multipart.FormData.BodyPart, Future[Future[String]]] = //TODO:return json?
    Flow[Multipart.FormData.BodyPart].async.map(part =>
      mapPart(path)(part)
    ).toMat(Sink.fold[Future[String], Future[String]](Future(""))((x, y) => x.flatMap(v1 => y.map(v2 => v1 + "~!~" + v2))))(Keep.right)

  def mapPart(path: List[String])(part: Multipart.FormData.BodyPart)(implicit materializer: Materializer): Future[String] = {
    part.filename match {
      case Some(name) => part.entity.dataBytes.runWith(writeFile(path, name))
        .map(res =>
          res.status match {
            case Success(_) => s"$name uploaded successfully"
            case Failure(s) => s"$name failed to upload for following reason:" + s
          })
      case _ => Future(s"${part.name} is not a file")
    }
  }

  private def writeFile(path: List[String], name: String) =
    FileIO.toPath(if (path.nonEmpty) getPath(path.head, path.tail :+ name: _*).path else getPath(name).path)

  protected def getPath(path: String, suffix: String*): File =
    File(Paths.get(curUserRootPath.toString, Seq(path) ++ suffix: _*))
}
