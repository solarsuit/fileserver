package com.solarsuit.akkacloud.controllers

import java.nio.file.Path

import better.files._
import com.solarsuit.akkacloud.NotTrowing.doNonThrowing
import com.solarsuit.akkacloud.controllers.helpers.Manage
import com.solarsuit.akkacloud.models.orm.{FileMapping, FileView, FileViewBuilder}


trait ManageImpl extends Manage {

  def getFileList(path: String): Option[List[FileView]] = doIoWithFile(path)(path => path.list.toList.map(FileViewBuilder(_, curUserRootPath)))

  protected def doIoWithFile[B](path: String)(action: File => B): Option[B] = doWithFile(path)(file => doNonThrowing(() => action(file)))

  def share(path: String): Option[String] = doWithFile(path)(file => FileMapping.provider.create(file.toString)).map(_.id)

  protected def doWithFile[B](path: String)(action: File => Option[B]): Option[B] = {
    for {
      p <- {
        val file = getPath(escape(path));
        if (file.exists) Some(file) else None
      }
      result <- action(p)
    } yield result
  }

  def unShare(path: String): Option[Unit] = doWithFile(path)(file => FileMapping.provider.seekByPath(file.toString)).flatMap(_.delete().fold(_ => None, _ => Some(Unit)))

  def delete(path: String): Option[Unit] = {
    unShare(path)
    doIoWithFile(path)(_.delete())
  }

  override def mkdir(path: String): Option[Unit] = doNonThrowing[Unit](getPath(escape(path)).createDirectory)

  protected def escape(s: String): String = {
    s.replaceAll("../|<|>", "")
  }

  protected def curUserRootPath: Path

  protected def getPath(path: String, suffix: String*): File

}


