package com.solarsuit.akkacloud.controllers

import akka.http.javadsl.model.headers.UserAgent
import akka.http.scaladsl.model.{ContentType, HttpResponse, StatusCodes}
import better.files.File
import com.solarsuit.akkacloud.controllers.helpers.FileResponce
import com.solarsuit.akkacloud.models.orm.FileMapping

object Download extends FileResponce {

  def getFile(id: String, userAgent: Option[UserAgent] = None): HttpResponse = {
    find(FileMapping.provider(id)) map (f => fileToStreamingResponce(f, userAgent)) match {
      case Some(data) => data
      case None => HttpResponse(StatusCodes.NotFound, Nil, "File Not Found")
    }
  }

  def getFileMeta(id: String): Option[(String, String)] =
    find(FileMapping.provider(id)) map (f => (f._1.name, f._2.toString()))


  private def find(f: Option[FileMapping]): Option[(File, ContentType)] =
    f.flatMap(mapping => if (mapping.getFile.exists) Some(mapping.getFile, mapping.contentType) else None)

}
