package com.solarsuit.akkacloud

import java.io.{FileInputStream, InputStream}
import java.nio.file.Paths
import java.security.{KeyStore, SecureRandom}
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}

import akka.actor.ActorSystem
import akka.http.javadsl.model.headers.UserAgent
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{path, _}
import akka.http.scaladsl.server.{PathMatcher, Route}
import akka.http.scaladsl.{Http, HttpsConnectionContext}
import akka.stream.ActorMaterializer
import com.solarsuit.akkacloud.controllers.helpers.WireServer
import com.solarsuit.akkacloud.controllers.{Download, ManageFiles}
import com.solarsuit.akkacloud.models.orm.User
import com.solarsuit.akkacloud.models.orm.mapdb.MapDbSchema

import scala.io.StdIn
import scala.util.{Failure, Success}


/**
  * Created by arsenii on 29.03.17.
  */
object WebServer {

  import com.typesafe.config.{Config, ConfigFactory}

  lazy val storagePath: String = conf.getString("storage.path")
  lazy val hostname: String = conf.getString("web.hostname")
  lazy val port: Int = conf.getInt("web.port")
  lazy val dbPath: String = conf.getString("db.path")
  lazy val staticPath: String = conf.getString("web.static")
  lazy val keyStorePath: String = conf.getString("web.keyPath")
  private lazy val conf: Config = ConfigFactory.parseFile(new java.io.File("application.conf"))
  private implicit val system = ActorSystem("my-system")
  private implicit val materializer = ActorMaterializer()

  def main(args: Array[String]) {

    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    val route: Route =
      pathPrefix("api") {
        authenticateBasic[User]("", User.auth) {
          implicit user =>
            path(Segments) { path =>
              manageWireRoute(path) ~
                manageUploadRoute(path) ~
                manageDownloadRoute(path)
            }

        }
      } ~ pathPrefix("download") {
        path("type" / Segment) { id =>
          get {
            Download.getFileMeta(id) match {
              case Some(meta) => complete(meta._1 + ":" + meta._2)
              case None => complete(HttpResponse(StatusCodes.NotFound))
            }
          }
        } ~ path("direct" / Segment) { id =>
          get {
            extractRequest(request => complete(Download.getFile(id, request.header[UserAgent])))
          }
        } ~ path(Segment) { id =>
          getFromFile(staticPath + "download.html")
        }
      } ~ path(Segment)(path => staticRoute(path)) ~ path(PathMatcher(""))(getFromFile(staticPath + "index.html"))

    Http().setDefaultServerHttpContext(https())
    val bindingFuture = Http().bindAndHandle(route, hostname, port)

    println(s"Server online at https://" + hostname + ":" + port + " \nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => {
      MapDbSchema.close()
      system.terminate()
    }) // and shutdown when done

  }

  def https(password: Array[Char] = "".toCharArray): _root_.akka.http.scaladsl.ConnectionContext = {
    val ks: KeyStore = KeyStore.getInstance("PKCS12")
    val keystore: InputStream = new FileInputStream(new java.io.File(keyStorePath))
    require(keystore != null, "Keystore required!")
    ks.load(keystore, password)
    val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, password)
    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    tmf.init(ks)
    val sslContext: SSLContext = SSLContext.getInstance("TLSv1.2")
    sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, SecureRandom.getInstanceStrong)
    new HttpsConnectionContext(sslContext, None, None, None, None, None)
  }

  //TODO:check if uploading link to somewhere allows to download file outside of folder
  def manageWireRoute(path: List[String])(implicit user: User): Route = put {
    extract(_.request.entity match {
      case HttpEntity.Strict(_: ContentType.Binary, data) => complete(new WireServer(user).process(path, data.toArray))
      case _ => complete(HttpResponse(StatusCodes.BadRequest)) //TODO:fail here?
    })(e => e)
  }

  def manageDownloadRoute(path: List[String])(implicit user: User): Route = get {
    new ManageFiles(user.id).download(Paths.get(path.head, path.tail: _*).toString) match {
      case Some(v) => complete(v)
      case None => complete(HttpResponse(StatusCodes.NotFound))
    }
  }

  def manageUploadRoute(path: List[String])(implicit user: User): Route = post {
    withoutSizeLimit {
      entity(as[Multipart.FormData]) { formData =>
        val result = formData.parts.runWith(new ManageFiles(user.id).partSink(path)).flatten
        onComplete(result) {
          case Success(s) => complete(s)
          case Failure(f) => complete(f)
        }
      }
    }
  }

  def staticRoute(path: String): _root_.akka.http.scaladsl.server.Route =
    getFromFile(staticPath + path) ~ getFromFile(staticPath + path + ".html")
}
