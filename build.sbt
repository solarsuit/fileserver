name := "akka-cloud-server"

organization := "com.solarsuit"

version := "1.0-SNAPSHOT"

scalaVersion := "2.12.0"

libraryDependencies ++= Seq(
  "com.lihaoyi" %% "autowire" % "0.2.6",
  "com.typesafe.akka" %% "akka-http" % "10.0.5",
  "org.mapdb" % "mapdb" % "3.0.3",
  "com.github.pathikrit" %% "better-files" % "2.17.1",
  "org.apache.logging.log4j" % "log4j-core" % "2.8",
  "io.suzaku" %% "boopickle" % "1.2.6",
  "com.google.api-client" % "google-api-client" % "1.22.0",
  "com.solarsuit" %% "akka-cloud-common" % "1.0-SNAPSHOT"
)


mainClass in Compile := Some("com.solarsuit.akkacloud.WebServer")